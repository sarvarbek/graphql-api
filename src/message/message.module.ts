import { Module } from '@nestjs/common';
import { MessageResolver } from './message.resolver';
import { MessageService } from './message.service';
import { TypeOrmModule } from "@nestjs/typeorm";
import { MessageEntity } from "./entity/message.entity";

@Module({
  imports: [TypeOrmModule.forFeature([MessageEntity])],
  providers: [MessageResolver, MessageService]
})
export class MessageModule {}
