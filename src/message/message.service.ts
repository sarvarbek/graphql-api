import { Injectable } from '@nestjs/common';
import { Repository } from "typeorm";
import { MessageEntity } from "./entity/message.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { AddMessageArgs } from "./args/add-message.args";


@Injectable()
export class MessageService {
  constructor(@InjectRepository(MessageEntity) public readonly messageRepository: Repository<MessageEntity>) {}

  async getMessage() : Promise<MessageEntity[]> {
    let message = await this.messageRepository.find()
    return message
  }

  async addMessage(addMessageArgs: AddMessageArgs): Promise<String> {
    let message : MessageEntity = new MessageEntity()
    message.text = addMessageArgs.text;
    message.priority = addMessageArgs.priority
    await this.messageRepository.save(message);
    return 'Message created';
  }
}
