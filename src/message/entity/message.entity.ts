import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({name: "message"})
export class MessageEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  text: string;

  @Column()
  priority: string;
}
