
/*
 * -------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export interface AddMessageArgs {
    text: string;
    priority: string;
}

export interface MessageSchema {
    id: number;
    text: string;
    priority: string;
}

export interface IQuery {
    message(): MessageSchema[] | Promise<MessageSchema[]>;
}

export interface IMutation {
    addMessages(adMessageArgs: AddMessageArgs): string | Promise<string>;
}

type Nullable<T> = T | null;
